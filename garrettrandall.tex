%-------------------------------------
% LaTeX Resume for Software Engineers
% Author : Leslie Cheng
% License : MIT
%-------------------------------------

\documentclass[letterpaper,11pt]{article}[leftmargin=*]

\usepackage[empty]{fullpage}
\usepackage{enumitem}
\usepackage{ifxetex}
\ifxetex
  \usepackage{fontspec}
  \usepackage[xetex]{hyperref}
\else
  \usepackage[utf8]{inputenc}
  \usepackage[T1]{fontenc}
  \usepackage[pdftex]{hyperref}
\fi
\usepackage{fontawesome}
\usepackage[sfdefault,light]{FiraSans}
\usepackage{anyfontsize}
\usepackage{xcolor}
\usepackage{tabularx}

%-------------------------------------------------- SETTINGS HERE --------------------------------------------------
% Header settings
\def \fullname {Garrett Randall}
\def \subtitle {}

\def \linkedinicon {\faLinkedin}
\def \linkedinlink {https://linkedin.com/in/garrandall}
\def \linkedintext {garrandall}

\def \phoneicon {\faPhone}
\def \phonetext {+1 226-932-2035}

\def \emailicon {\faEnvelope}
\def \emaillink {mailto:garrett.d.randall@gmail.com}
\def \emailtext {garrett.d.randall@gmail.com}

\def \gitlabicon {\faGitlab}
\def \gitlablink {https://gitlab.com/garrandall}
\def \gitlabtext {garrandall}

\def \websiteicon {\faGlobe}
\def \websitelink {https://garrandall.com/}
\def \websitetext {garrandall.com}

\def \headertype {\doublecol} % \singlecol or \doublecol

% Misc settings
\def \entryspacing {-2pt}

\def \bulletstyle {\faAngleRight}

% Define colours
\definecolor{primary}{HTML}{000000}
\definecolor{secondary}{HTML}{0D47A1}
\definecolor{accent}{HTML}{263238}
\definecolor{links}{HTML}{1565C0}

%-------------------------------------------------------------------------------------------------------------------

% Defines to make listing easier
\def \linkedin {\hfill\href{\linkedinlink}{\linkedintext}\hspace{3pt}\linkedinicon}
\def \phone {\hfill{\phonetext}\hspace{3pt}\phoneicon}
\def \email {\hfill\href{\emaillink}{\emailtext}\hspace{3pt}\emailicon}
\def \gitlab {\hfill\href{\gitlablink}{\gitlabtext}\hspace{3pt}\gitlabicon}
\def \website {\hfill\href{\websitelink}{\websitetext}\hspace{3pt}\websiteicon}

% Adjust margins
\addtolength{\oddsidemargin}{-0.55in}
\addtolength{\evensidemargin}{-0.55in}
\addtolength{\textwidth}{1.1in}
\addtolength{\topmargin}{-0.6in}
\addtolength{\textheight}{1.1in}

% Define the link colours
\hypersetup{
    colorlinks=true,
    urlcolor=links,
}

% Set the margin alignment
\raggedbottom
\raggedright
\setlength{\tabcolsep}{0in}

%-------------------------
% Custom commands

% Sections
\renewcommand{\section}[2]{\vspace{2pt}
  \colorbox{secondary}{\color{white}\raggedbottom\normalsize\textbf{{#1}{\hspace{5pt}#2}}}
}

% Entry start and end, for spacing
\newcommand{\resumeEntryStart}{\begin{itemize}[leftmargin=2.5mm]}
\newcommand{\resumeEntryEnd}{\end{itemize}\vspace{\entryspacing}}

% Itemized list for the bullet points under an entry, if necessary
\newcommand{\resumeItemListStart}{\begin{itemize}[leftmargin=4.5mm]}
\newcommand{\resumeItemListEnd}{\end{itemize}}

% Resume item
\renewcommand{\labelitemii}{\bulletstyle}
\newcommand{\resumeItem}[1]{
  \item\small{
    {#1 \vspace{-3pt}}
  }
}

% Entry with title, subheading, date(s), and location
\newcommand{\resumeEntryTSDL}[4]{
  \vspace{-5pt}\item[]
    \begin{tabularx}{0.97\textwidth}{X@{\hspace{60pt}}r}
      \textbf{\color{primary}#1} & {\firabook\color{accent}\small#2} \\
      \textit{\color{accent}\small#3} & \textit{\color{accent}\small#4} \\
    \end{tabularx}\vspace{-6pt}
}

% Entry with title and date(s)
\newcommand{\resumeEntryTD}[2]{
  \vspace{-1pt}\item[]
    \begin{tabularx}{0.97\textwidth}{X@{\hspace{60pt}}r}
      \textbf{\color{primary}#1} & {\firabook\color{accent}\small#2} \\
    \end{tabularx}\vspace{-6pt}
}

% Entry for special (skills)
\newcommand{\resumeEntryS}[2]{
  \item[]\small{
    \textbf{\color{primary}#1 }{ #2 \vspace{-6pt}}
  }
}

% Double column header
\newcommand{\doublecol}[6]{
  \begin{tabularx}{\textwidth}{Xr}
    {
      \begin{tabular}[c]{l}
        \fontsize{35}{45}\selectfont{\color{primary}{{\textbf{\fullname}}}} \\
        {\textit{\subtitle}} % You could add a subtitle here
      \end{tabular}
    } & {
      \begin{tabular}[c]{l@{\hspace{1.5em}}l}
        {\small#4} & {\small#1} \\
        {\small#5} & {\small#2} \\
        {\small#6} & {\small#3}
      \end{tabular}
    }
  \end{tabularx}
}

% Single column header
\newcommand{\singlecol}[6]{
  \begin{tabularx}{\textwidth}{Xr}
    {
      \begin{tabular}[b]{l}
        \fontsize{35}{45}\selectfont{\color{primary}{{\textbf{\fullname}}}} \\
        {\textit{\subtitle}} % You could add a subtitle here
      \end{tabular}
    } & {
      \begin{tabular}[c]{l}
        {\small#1} \\
        {\small#2} \\
        {\small#3} \\
        {\small#4} \\
        {\small#5} \\
        {\small#6}
      \end{tabular}
    }
  \end{tabularx}
}

\begin{document}
%-------------------------------------------------- BEGIN HERE --------------------------------------------------

%---------------------------------------------------- HEADER ----------------------------------------------------

\headertype{\linkedin}{\gitlab}{\website}{\email}{\phone}{} % Set the order of items here
\vspace{-10pt} % Set a negative value to push the body up, and the opposite

%-------------------------------------------------- PROGRAMMING SKILLS --------------------------------------------------
\section{\faGears}{Skills}
 \resumeEntryStart
  \resumeEntryS{Languages } {Python, C\#, Java, SQL, JavaScript, React, Ruby}
  \resumeEntryS{Tools } {AWS (Lambda, EC2, Kinesis, S3, IAM), Kubernetes, Docker, Jenkins, Apache Kafka}
 \resumeEntryEnd


%-------------------------------------------------- EXPERIENCE --------------------------------------------------
\section{\faPieChart}{Work Experience}

  \resumeEntryStart
  \resumeEntryTSDL
    {Magnet Forensics}{Jul. 2021 -- Present}
    {Software Developer}{Waterloo, ON}
  \resumeItemListStart
    \resumeItem {Worked on Magnet Review, a SAAS evidence review platform (with customer-hosted offerings) that stores and serves millions of digital evidence items in convenient views for forensic investigators worldwide}
    \resumeItem {Designed and developed secure RESTful APIs and asynchronous messaging flows in .NET Core powering web features such as conversation view and real-time case status}
    \resumeItem {Modified internal Kubernetes manifests and Helm charts to support new features in SAAS and customer-hosted environments}
    \resumeItem {Implemented policy-based authorization features, such as roles and two-layer distributed policy caching, in a flexible authorization system powering data sharing and customer tenant administration}
    \resumeItem {Created frontend components in React adhering to UX designs and accessibility standards, such as tabular views and a popover colour picker with keyboard controls}
    \resumeItem {Drove the use of Argo Workflows and the Locust load-testing framework to perform system performance reports and identify API optimizations}
  \resumeItemListEnd
  \resumeEntryEnd

  \resumeEntryStart
    \resumeEntryTSDL
      {Capital One}{Jan. 2020 -- Jul. 2021}
      {Associate Software Developer}{Kitchener, ON}
    \resumeItemListStart
      \resumeItem {Worked on Secondlook, a suite of real-time alerts delivering account insights to customers via email and in-app notifications, such as duplicate charges and sudden bill increases}
      \resumeItem {Engineered real-time AWS Lambda/Kinesis pipelines to process incoming transactions, integrate with enterprise web APIs, and generate insights, decommissioning legacy overnight batch processes and decreasing alert time from 3 days to 30 seconds}
      \resumeItem {Contributed to standard internal tooling in Python and Java, including enterprise-compliant IaC projects using boto3 and Terraform, and streaming data testing frameworks to greatly improve development velocity}
      \resumeItem {As a member of the small Labs team, deployed data engineering tools such a React web app for interfacing with S3, and data-loading AWS Lambda functions in Python to empower data scientists}
      \resumeItem {Interviewed and mentored several software engineering co-op students in 4 month terms as part of a small, focused team}
    \resumeItemListEnd
  \resumeEntryEnd

  \resumeEntryStart
    \resumeEntryTSDL
      {Flipp}{Jan. 2019 -- Apr. 2019}
      {Software Developer Co-op}{Toronto, ON}
    \resumeItemListStart
      \resumeItem {Contributed to the Item Data Platform, a scalable Kafka-based platform powering Flipp's repository of billions of e-commerce items, written with NodeJS, Ruby, and Scala}
      \resumeItem {On-call for maintenance of hundreds of unique Ruby-based e-commerce indexers to maintain up-to-date e-commerce product information}
      \resumeItem {Implemented features, performed database migrations, and fixed bugs in the legacy item data system}
    \resumeItemListEnd
  \resumeEntryEnd

%-------------------------------------------------- EDUCATION --------------------------------------------------
\section{\faGraduationCap}{Education}

  \resumeEntryStart
    \resumeEntryTSDL
      {University of Waterloo}{2014 -- 2019}
      {BCS Honours Computer Science -- History Minor -- Co-operative Program}{Waterloo, ON}
  \resumeEntryEnd

%-------------------------------------------------- PROJECTS --------------------------------------------------
\section{\faFlask}{Projects}

  \resumeEntryStart
    \resumeEntryTD
      {Generative Art in WebGL}{\href{https://gen.garrandall.com}{gen.garrandall.com}}
    \resumeItemListStart
      \resumeItem {Implemented graphical generative art demos in the browser, including cellular automata and flow fields}
      \resumeItem {Drawn with WebGL utilizing techniques like compute shaders, and deployed with Webpack and Vercel}
    \resumeItemListEnd
  \resumeEntryEnd

\end{document}
